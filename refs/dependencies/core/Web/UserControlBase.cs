using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using LevelOne.Core.Security;

namespace LevelOne.Core.Web
{
    /// <summary>
    /// TODO
    /// </summary>
    public class UserControlBase : System.Web.UI.UserControl
    {
        /// <summary>
        /// Logger to be used within child classes
        /// </summary>
        

        /// <summary>
        /// User Identity in the current context
        /// </summary>
        private IIdentity userIdentity;

        /// <summary>
        /// User Identity in the current context
        /// </summary>
        protected virtual IIdentity UserIdentity
        {
            get { return userIdentity; }
            set { userIdentity = value; }
        }

        /// <summary>
        /// User Identity in the current context
        /// </summary>
        protected CustomIdentity CustomUserIdentity
        {
            get { return (CustomIdentity) userIdentity; }
        }

        /// <summary>
        /// TODO - comments
        /// </summary>
        public UserControlBase()
        {
            // create a logger
           
            // get the user context
            userIdentity = Context.User.Identity;
        }


        /// <summary>
        /// OnError is overridden so that the page can save all error context
        /// information to the application cache.  A custom error page can
        /// then retrieve it and display the information to the user, write
        /// it to the event log, etc.
        /// </summary>
        /// <param name="e">Event arguments</param>
        /// <remarks>By default, this method will store the following
        /// information in a hash table and place it in the application cache.
        /// <list type="table">
        ///    <listheader>
        ///       <term>Hash Table Key</term>
        ///       <description>Description</description>
        ///    </listheader>
        ///    <item>
        ///       <term>LastError</term>
        ///       <description>This element contains a string that represents
        /// the information returned from
        /// <b>Server.GetLastError().ToString()</b>.</description>
        ///    </item>
        ///    <item>
        ///       <term>ServerVars</term>
        ///       <description>This element contains a <b>SortedList</b> that
        /// contains the following entries from the
        /// <b>Request.ServerVariables</b> collection:
        ///     <list type="bullet">
        ///         <item>SCRIPT_NAME</item>
        ///         <item>HTTP_HOST</item>
        ///         <item>HTTP_USER_AGENT</item>
        ///         <item>AUTH_TYPE</item>
        ///         <item>AUTH_USER</item>
        ///         <item>LOGON_USER</item>
        ///         <item>SERVER_NAME</item>
        ///         <item>LOCAL_ADDR</item>
        ///         <item>REMOTE_ADDR</item>
        ///     </list>
        ///       </description>
        ///    </item>
        ///    <item>
        ///       <term>QueryString</term>
        ///       <description>This element contains a copy of the
        /// <b>Request.QueryString</b> collection.</description>
        ///    </item>
        ///    <item>
        ///       <term>Form</term>
        ///       <description>This element contains a copy of the
        /// <b>Request.Form</b> collection.</description>
        ///    </item>
        ///    <item>
        ///       <term>Page</term>
        ///       <description>This contains a string representing the name
        /// of the page that caused the error as returned by
        /// <b>Request.Path</b>.</description>
        ///    </item>
        /// </list>
        /// <p/>The hash table is stored in the application cache using the
        /// current user's remote address as obtained by the <b>REMOTE_ADDR</b>
        /// server variable.  The entries are stored with a time limit of
        /// five minutes so that it doesn't hold resources for too long.
        /// The custom error page can also delete the object from the cache
        /// once it has retrieved it.  This method should work well for most
        /// applications.  Unless you are expecting an extremely large number
        /// of users and there was an unexpected error that everyone got, it
        /// shouldn't put much of a load on the server.</remarks>
        protected override void OnError(System.EventArgs e)
        {
            string remoteAddr;

            Hashtable htErrorContext = new Hashtable(5);
            SortedList<String, String> slServerVars = new SortedList<String, String>(9);

            // Extract a subset of the server variables
            slServerVars["SCRIPT_NAME"] = Request.ServerVariables["SCRIPT_NAME"];
            slServerVars["HTTP_HOST"] = Request.ServerVariables["HTTP_HOST"];
            slServerVars["HTTP_USER_AGENT"] = Request.ServerVariables["HTTP_USER_AGENT"];
            slServerVars["AUTH_USER"] = Request.ServerVariables["AUTH_USER"];
            slServerVars["LOGON_USER"] = Request.ServerVariables["LOGON_USER"];
            slServerVars["SERVER_NAME"] = Request.ServerVariables["SERVER_NAME"];
            slServerVars["LOCAL_ADDR"] = Request.ServerVariables["LOCAL_ADDR"];
            remoteAddr = Request.ServerVariables["REMOTE_ADDR"];
            slServerVars["REMOTE_ADDR"] = remoteAddr;

            // Save the context information
            htErrorContext["LastError"] = Server.GetLastError().ToString();
            htErrorContext["ServerVars"] = slServerVars;
            htErrorContext["QueryString"] = Request.QueryString;
            htErrorContext["Form"] = Request.Form;
            htErrorContext["Page"] = Request.Path;

            // Store it in the cache with a short time limit.  The remote
            // address is used as a key.  We can't use the session ID or store
            // the info in the session as it's not always the same session on
            // the error page.
            Cache.Insert(remoteAddr, htErrorContext,
                null, DateTime.MaxValue, TimeSpan.FromMinutes(5));


            

            base.OnError(e);
        }

        /// <summary>
        /// Recursively search for control from a parent
        /// </summary>
        /// <param name="root"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        protected Control FindControlRecursive(Control root, string id)
        {
            if (root.ID == id)
            {
                return root;
            }

            foreach (Control c in root.Controls)
            {
                Control t = FindControlRecursive(c, id);
                if (t != null)
                {
                    return t;
                }
            }

            return null;
        }
       
        
    }
}
