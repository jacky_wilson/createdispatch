using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LevelOne.Core.Web
{
    /// <summary>
    /// This is a common base class for ASP.NET pages.
    /// </summary>
    /// <remarks>By using this class as the base class for your web pages, you
    /// can reduce the amount of code and HTML that you have to write and also
    /// take advantage of the features provided by the class to make your pages
    /// more robust with regard to control focus, control enabling and
    /// disabling, checking for changed field data before leaving a page,
    /// error handling, etc.</remarks>
    public abstract class PageBase : System.Web.UI.Page
    {
        /// <summary>
        /// Recursively search for control from a parent
        /// </summary>
        /// <param name="root"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        protected Control FindControlRecursive(Control root, string id)
        {
            if (root.ID == id)
            {
                return root;
            }

            foreach (Control c in root.Controls)
            {
                Control t = FindControlRecursive(c, id);
                if (t != null)
                {
                    return t;
                }
            }

            return null;
        }

 
        //=====================================================================
        // Properties

        /// <summary>
        /// URL of FogBugz or other url taking error notification
        /// </summary>
        protected abstract String BugServerURL
        {
            get;
        }

        /// <summary>
        /// FogBugz user or other user on server taking error notification
        /// </summary>
        protected abstract String BugServerUser
        {
            get;
        }

        /// <summary>
        /// URL of FogBugz or other url taking error notification
        /// </summary>
        protected abstract String BugServerProject
        {
            get;
        }

        protected abstract String BugServerArea
        {
            get;
        }

        protected abstract String BugDefaultMessage
        {
            get;
        }

        protected virtual void SubmitBug()
        {
        }



        /// <summary>
        /// This property can be used to get the current user ID without the
        /// domain qualifier if one is present.
        /// </summary>
        /// <remarks>This property is only useful when using basic or
        /// integrated security with your web application.  It is useful
        /// for auditing purposes or looking up security related information
        /// and saves you from having to manually remove the domain name from
        /// the user ID.</remarks>
        /// <value>Returns the value of the <b>User.Identity.Name</b>
        /// property without the domain qualifier.  For example if it
        /// is <b>MYDOMAIN\EWOODRUFF</b>, this property returns
        /// <b>EWOODRUFF</b>.</value>
        [Browsable(false)]
        public string CurrentUser
        {
            get
            {
                // This prevents an exception being reported in design view
                if (this.Context == null)
                    return null;

                string strUser = User.Identity.Name;
                int nPos = strUser.IndexOf('\\');

                if (nPos != -1)
                    strUser = strUser.Substring(nPos + 1);

                return strUser;
            }
        }



        /// <summary>
        /// OnError is overridden so that the page can save all error context
        /// information to the application cache.  A custom error page can
        /// then retrieve it and display the information to the user, write
        /// it to the event log, etc.
        /// </summary>
        /// <param name="e">Event arguments</param>
        /// <remarks>By default, this method will store the following
        /// information in a hash table and place it in the application cache.
        /// <list type="table">
        ///    <listheader>
        ///       <term>Hash Table Key</term>
        ///       <description>Description</description>
        ///    </listheader>
        ///    <item>
        ///       <term>LastError</term>
        ///       <description>This element contains a string that represents
        /// the information returned from
        /// <b>Server.GetLastError().ToString()</b>.</description>
        ///    </item>
        ///    <item>
        ///       <term>ServerVars</term>
        ///       <description>This element contains a <b>SortedList</b> that
        /// contains the following entries from the
        /// <b>Request.ServerVariables</b> collection:
        ///     <list type="bullet">
        ///         <item>SCRIPT_NAME</item>
        ///         <item>HTTP_HOST</item>
        ///         <item>HTTP_USER_AGENT</item>
        ///         <item>AUTH_TYPE</item>
        ///         <item>AUTH_USER</item>
        ///         <item>LOGON_USER</item>
        ///         <item>SERVER_NAME</item>
        ///         <item>LOCAL_ADDR</item>
        ///         <item>REMOTE_ADDR</item>
        ///     </list>
        ///       </description>
        ///    </item>
        ///    <item>
        ///       <term>QueryString</term>
        ///       <description>This element contains a copy of the
        /// <b>Request.QueryString</b> collection.</description>
        ///    </item>
        ///    <item>
        ///       <term>Form</term>
        ///       <description>This element contains a copy of the
        /// <b>Request.Form</b> collection.</description>
        ///    </item>
        ///    <item>
        ///       <term>Page</term>
        ///       <description>This contains a string representing the name
        /// of the page that caused the error as returned by
        /// <b>Request.Path</b>.</description>
        ///    </item>
        /// </list>
        /// <p/>The hash table is stored in the application cache using the
        /// current user's remote address as obtained by the <b>REMOTE_ADDR</b>
        /// server variable.  The entries are stored with a time limit of
        /// five minutes so that it doesn't hold resources for too long.
        /// The custom error page can also delete the object from the cache
        /// once it has retrieved it.  This method should work well for most
        /// applications.  Unless you are expecting an extremely large number
        /// of users and there was an unexpected error that everyone got, it
        /// shouldn't put much of a load on the server.</remarks>
        protected override void OnError(System.EventArgs e)
        {
            string remoteAddr;

            Hashtable htErrorContext = new Hashtable(5);
            SortedList slServerVars = new SortedList(9);

            // Extract a subset of the server variables
            slServerVars["SCRIPT_NAME"] = Request.ServerVariables["SCRIPT_NAME"];
            slServerVars["HTTP_HOST"] = Request.ServerVariables["HTTP_HOST"];
            slServerVars["HTTP_USER_AGENT"] = Request.ServerVariables["HTTP_USER_AGENT"];
            slServerVars["AUTH_USER"] = Request.ServerVariables["AUTH_USER"];
            slServerVars["LOGON_USER"] = Request.ServerVariables["LOGON_USER"];
            slServerVars["SERVER_NAME"] = Request.ServerVariables["SERVER_NAME"];
            slServerVars["LOCAL_ADDR"] = Request.ServerVariables["LOCAL_ADDR"];
            remoteAddr = Request.ServerVariables["REMOTE_ADDR"];
            slServerVars["REMOTE_ADDR"] = remoteAddr;

            // Save the context information
            htErrorContext["LastError"] = Server.GetLastError().ToString();
            htErrorContext["ServerVars"] = slServerVars;
            htErrorContext["QueryString"] = Request.QueryString;
            htErrorContext["Form"] = Request.Form;
            htErrorContext["Page"] = Request.Path;

            // Store it in the cache with a short time limit.  The remote
            // address is used as a key.  We can't use the session ID or store
            // the info in the session as it's not always the same session on
            // the error page.
            Cache.Insert(remoteAddr, htErrorContext,
                null, DateTime.MaxValue, TimeSpan.FromMinutes(5));

            if (ConfigurationManager.AppSettings["submitBugz"] == "true")
            {
                SubmitBug();
            }

            base.OnError(e);
        }


    }
}
