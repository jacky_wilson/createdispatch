using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

namespace LevelOne.Core.DAL
{
    /// <summary>
    /// Generical Data Access Layer
    /// </summary>
    /// <typeparam name="TData">The type of the data.</typeparam>
    /// <typeparam name="TDataCollection">The type of the data collection.</typeparam>
	public abstract class GenericDalBase<TData, TDataCollection> : DalBase
		where TDataCollection : List<TData>
		where TData : class
	{       

		// disallow normal instantiation 
		protected GenericDalBase() { }

        /// <summary>
        /// Populates the specified generic data object with data
        /// from the SQL call.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <returns></returns>
		protected abstract TData Populate(SqlDataReader reader);
        //protected abstract TData Populate(TData param);

        #region Get Commands

        protected abstract SqlCommand GetListSqlCommand();
		protected abstract SqlCommand GetSqlCommand(int id);
        protected virtual SqlCommand GetSqlCommand(long id) {throw new NotImplementedException();}

        #endregion

        #region Insert Commands

        protected abstract SqlCommand InsertSqlCommand(TData param);

        protected virtual SqlCommand InsertSqlCommand(TData param, int userId)
        {
            throw new NotImplementedException();
        }

        protected virtual SqlCommand InsertSqlCommand(TData param, long userId)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Update Commands


        protected virtual SqlCommand UpdateSqlCommand(TData param)
        {
            throw new NotImplementedException();
        }

        protected virtual SqlCommand UpdateSqlCommand(TData param, int userId)
        {
            throw new NotImplementedException();
        }

        protected virtual SqlCommand UpdateSqlCommand(TData param, long userId)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete Commands


        protected virtual SqlCommand DeleteSqlCommand(int id)
        {
            throw new NotImplementedException();
        }

        protected virtual SqlCommand DeleteSqlCommand(int id, int userId)
        {
            throw new NotImplementedException();
        }

        protected virtual SqlCommand DeleteSqlCommand(int id, long userId)
        {
            throw new NotImplementedException();
        }

        protected virtual SqlCommand DeleteSqlCommand(long id)
        {
            throw new NotImplementedException();
        }

        protected virtual SqlCommand DeleteSqlCommand(long id, int userId)
        {
            throw new NotImplementedException();
        }

        protected virtual SqlCommand DeleteSqlCommand(long id, long userId)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Get Methods
        
        /// <summary>
		/// Use for Selection stored procedures
		/// </summary>
		/// <returns></returns>
        public TDataCollection GetList()
		{
			TDataCollection returnList = Activator.CreateInstance(typeof(TDataCollection)) as TDataCollection;
			// I recommend stored procedures, but parameters help protect against SQL injection. 
			// Never concantonate strings sent to SQL unless you know what you are concatonating 
			// is tamperproof
			SqlCommand command = GetListSqlCommand();

            using (base.Connection)
            {
                base.Connection.Open();
                using(SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        TData data = Populate(reader);
                        returnList.Add(data);
                    }
                }
            }

			return returnList;
		}


        /// <summary>
		/// Use for Selection stored procedures
		/// </summary>
		/// <returns></returns>
        public TDataCollection GetList(string connectionString)
		{
			TDataCollection returnList = Activator.CreateInstance(typeof(TDataCollection)) as TDataCollection;
			// I recommend stored procedures, but parameters help protect against SQL injection. 
			// Never concantonate strings sent to SQL unless you know what you are concatonating 
			// is tamperproof
			SqlCommand command = GetListSqlCommand();

            using(base.Connection)
            {
                base.Connection.ConnectionString = connectionString;
                base.Connection.Open();
                using(SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        TData data = Populate(reader);
                        returnList.Add(data);
                    }
                }

            }

			return returnList;
		}//GetList

		/// <summary>
		/// Get Procedure based on id being int32
		/// </summary>
		/// <param name="id">int</param>
		/// <returns>TData</returns>
		public TData GetData(int id)
		{
			TData data = null;
			// I recommend stored procedures, but parameters help protect against SQL injection. 
			// Never concantonate strings sent to SQL unless you know what you are concatonating 
			// is tamperproof
			SqlCommand command = GetSqlCommand(id);
			using(base.Connection)
			{
				base.Connection.Open();
				using(SqlDataReader reader = command.ExecuteReader())
                {
				    if (reader.Read())
				    {
					    data = Populate(reader);
				    }
                }
			}

			return data;
		}

        /// <summary>
        /// Get Procedure based on id being int64
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>TData</returns>
        public  TData GetData(long id)
        {
            TData data = null;
            // I recommend stored procedures, but parameters help protect against SQL injection. 
            // Never concantonate strings sent to SQL unless you know what you are concatonating 
            // is tamperproof
            SqlCommand command = GetSqlCommand(id);
            using(base.Connection)
            {
                base.Connection.Open();
                using(SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        data = Populate(reader);
                    }
                }
            }

            return data;
        }

        #endregion
        
        #region Insert Methods
        
        /// <summary>
        /// Insert Stored procedure  
        /// </summary>
        /// <param name="param">TData = "Table Name"Info. param = variables for columns of that table
        /// </param>
        public virtual int Insert(TData param)
        {
            SqlCommand command = InsertSqlCommand(param);
            int id = 0;
            using(base.Connection)
            {
                base.Connection.Open();
                //command.ExecuteNonQuery();
                using(SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        id = GetInt32Internal(reader, 0);
                    }
                }
                                
            }
            return id;
        }

        /// <summary>
        /// Insert Stored procedure  
        /// </summary>
        /// <param name="param">TData = "Table Name"Info. param = variables for columns of that table
        /// </param>
        public virtual int Insert(TData param, int userId)
        {
            SqlCommand command = InsertSqlCommand(param, userId);
            int id = 0;
            using(base.Connection)
            {
                base.Connection.Open();
                //command.ExecuteNonQuery();
                using(SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        id = GetInt32Internal(reader, 0);
                    }
                }

            }
            return id;
        }

        /// <summary>
        /// Inserts Stored Procudure for logged inserts with BigInt user id's
        /// </summary>
        /// <param name="param">The param.</param>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        public virtual int Insert(TData param, long userId)
        {
            SqlCommand command = InsertSqlCommand(param, userId);
            int id = 0;
            using(base.Connection)
            {
                base.Connection.Open();
                //command.ExecuteNonQuery();
                using(SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        id = GetInt32Internal(reader, 0);
                    }
                }

            }
            return id;
        }

        #endregion

        #region Update Methods
        
        /// <summary>
        /// Update Stored Procedure
        /// </summary>
        /// <param name="param">TData = "Table Name"Info. param = variables for columns of that table
        /// </param>
        public virtual void Update(TData param)
        {
            SqlCommand command = UpdateSqlCommand(param);
            using(base.Connection)
            {
                base.Connection.Open();
                command.ExecuteNonQuery();

            }
        }

        /// <summary>
        /// Update Stored Procedure
        /// </summary>
        /// <param name="param">TData = "Table Name"Info. param = variables for columns of that table</param>
        /// <param name="userId">The user id.</param>
        public virtual void Update(TData param, int userId)
        {
            SqlCommand command = UpdateSqlCommand(param, userId);
            using(base.Connection)
            {
                base.Connection.Open();
                command.ExecuteNonQuery();

            }
        }

        /// <summary>
        /// Update Stored Procedure
        /// </summary>
        /// <param name="param">TData = "Table Name"Info. param = variables for columns of that table</param>
        /// <param name="userId">The user id.</param>
        public virtual void Update(TData param, long userId)
        {
            SqlCommand command = UpdateSqlCommand(param, userId);
            using(base.Connection)
            {
                base.Connection.Open();
                command.ExecuteNonQuery();

            }
        }

        #endregion

        #region Delete Methods
        
        /// <summary>
        /// Delete Stored Procedure
        /// </summary>
        /// <param name="param">id of data to delete
        /// </param>
        public virtual void Delete(int id)
        {
            SqlCommand command = DeleteSqlCommand(id);
            using(base.Connection)
            {
                base.Connection.Open();
                command.ExecuteNonQuery();

            }
        }

        /// <summary>
        /// Delete Stored Procedure
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="userId">The user id.</param>
        public virtual void Delete(int id, int userId)
        {
            SqlCommand command = DeleteSqlCommand(id, userId);
            using(base.Connection)
            {
                base.Connection.Open();
                command.ExecuteNonQuery();

            }
        }

        /// <summary>
        /// Delete Stored Procedure
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="userId">The user id.</param>
        public virtual void Delete(int id, long userId)
        {
            SqlCommand command = DeleteSqlCommand(id, userId);
            using(base.Connection)
            {
                base.Connection.Open();
                command.ExecuteNonQuery();

            }
        }

        /// <summary>
        /// Delete Stored Procedure
        /// </summary>
        /// <param name="param">id of data to delete
        /// </param>
        public virtual void Delete(long id)
        {
            SqlCommand command = DeleteSqlCommand(id);
            using(base.Connection)
            {
                base.Connection.Open();
                command.ExecuteNonQuery();

            }
        }

        /// <summary>
        /// Delete Stored Procedure
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="userId">The user id.</param>
        public virtual void Delete(long id, int userId)
        {
            SqlCommand command = DeleteSqlCommand(id, userId);
            using(base.Connection)
            {
                base.Connection.Open();
                command.ExecuteNonQuery();

            }
        }

        /// <summary>
        /// Delete Stored Procedure
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="userId">The user id.</param>
        public virtual void Delete(long id, long userId)
        {
            SqlCommand command = DeleteSqlCommand(id, userId);
            using(base.Connection)
            {
                base.Connection.Open();
                command.ExecuteNonQuery();

            }
        }

        #endregion

        #region Stored Procedure Helpers
        
        /// <summary>
        /// Execute procedure return multiple results
        /// </summary>
        /// <returns></returns>
        protected TDataCollection ExecuteQueryMultipleResults(SqlCommand command)
        {
            TDataCollection returnList = Activator.CreateInstance(typeof(TDataCollection)) as TDataCollection;
            
            using(base.Connection)
            {
                base.Connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        TData data = Populate(reader);
                        returnList.Add(data);
                    }
                }

            }
            return returnList;
        }

        /// <summary>
        /// Execute procedure return single result
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>TData</returns>
        protected TData ExecuteQuerySingleResult(SqlCommand command)
        {
            TData data = null;

            using(base.Connection)
            {
                base.Connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        data = Populate(reader);
                    }
                }
            }
            return data;
        }


        /// <summary>
        /// Execute procedure return single result
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>TData</returns>
        protected void ExecuteNonQuery(SqlCommand command)
        {
            using(base.Connection)
            {
                base.Connection.Open();
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}
