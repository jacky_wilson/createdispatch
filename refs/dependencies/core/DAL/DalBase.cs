using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;


namespace LevelOne.Core.DAL
{
	/// <summary>
	/// A sample base class. A grown-up one would include support for transactions tracing, etc.
	/// and may also use factories to allow different back ends
	/// </summary>
	public abstract class DalBase :IDisposable
	{
		// Set up logger
		

        /// <summary>
        /// Initializes a new instance of the <see cref="DalBase"/> class.
        /// </summary>
		public DalBase()
		{
			
		}


        /// <summary>
        /// Database Connection String
        /// </summary>
		protected string connectionString = ConfigurationManager.AppSettings["primaryDBConn"];

        /// <summary>
        /// Database Connection
        /// </summary>
		private SqlConnection connection;

        /// <summary>
        /// Gets the database connection.
        /// </summary>
        /// <value>The connection.</value>
		protected SqlConnection Connection
		{
			get
			{
				if (this.connection == null || String.IsNullOrEmpty(this.connection.ConnectionString))
				{
					this.connection = new SqlConnection(connectionString);
				}
				return this.connection;
			}
		}

        /// <summary>
        /// Creates the procedure.
        /// </summary>
        /// <param name="sproc">The sproc.</param>
        /// <returns></returns>
		public System.Data.SqlClient.SqlCommand CreateProcedure(string sproc)
		{
			SqlCommand sqlCommand = new SqlCommand(sproc, this.Connection);
			sqlCommand.CommandType = System.Data.CommandType.StoredProcedure; ;
			return sqlCommand;
		}


        /// <summary>
        /// Gets the index of the column.
        /// </summary>
        /// <param name="reader">The reader for the sql result.</param>
        /// <param name="columnName">Name of the column for which to find oordinal in results.</param>
        /// <returns>Returns the column index if found. If not found, returns -1</returns>
        private static int GetColumnIndex(SqlDataReader reader, string columnName)
        {
            int index = 0;
            if (HasColumn(reader, columnName))
            {
                index = reader.GetOrdinal(columnName);
            }
            else
            {
                
                index = -1;
            }

            return index;
        }

        /// <summary>
        /// Gets the string value of column.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
		protected static string GetStringInternal(SqlDataReader reader, int index)
		{
			if (reader == null)
			{
				throw new ArgumentException("The reader passed to GetStringInternal can not be null");
			}
			if (reader.IsDBNull(index))
			{
				return null;
			}
			else
			{
				return reader.GetString(index);
			}
		}

        /// <summary>
        /// Gets the string value of column.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <returns></returns>
		protected static string GetStringInternal(SqlDataReader reader, string columnName)
		{
            int index = GetColumnIndex(reader, columnName);
			//int index = reader.GetOrdinal(columnName);
			return index < 0 ? null: GetStringInternal(reader, index);
		}

        /// <summary>
        /// Gets the int16 value of column.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
		protected static int GetInt16Internal(SqlDataReader reader, int index)
		{
			if (reader == null)
			{
				throw new ArgumentException("The reader passed to GetIntInternal can not be null");
			}
			if (reader.IsDBNull(index))
			{
				return 0;
			}
			else
			{
				return reader.GetInt16(index);
			}
		}

        /// <summary>
        /// Gets the int16 value of column.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <returns></returns>
		protected static int GetInt16Internal(SqlDataReader reader, string columnName)
		{
            int index = GetColumnIndex(reader, columnName);

			return index < 0 ? 0 : GetInt16Internal(reader, index);
		}

        /// <summary>
        /// Gets the int32 value of column.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
		protected static int GetInt32Internal(SqlDataReader reader, int index)
		{
			if (reader == null)
			{
				throw new ArgumentException("The reader passed to GetIntInternal can not be null");
			}
			if (reader.IsDBNull(index))
			{
				return 0;
			}
			else
			{
				return reader.GetInt32(index);
			}
		}

        /// <summary>
        /// Gets the int32 value of column.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <returns></returns>
		protected static int GetInt32Internal(SqlDataReader reader, string columnName)
		{
            int index = GetColumnIndex(reader, columnName);
			
			return index < 0 ? 0 : GetInt32Internal(reader, index);
		}



        /// <summary>
        /// Gets the int64 value of column.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
		protected static long GetInt64Internal(SqlDataReader reader, int index)
		{
			if (reader == null)
			{
				throw new ArgumentException("The reader passed to GetIntInternal can not be null");
			}
			if (reader.IsDBNull(index))
			{
				return 0;
			}
			else
			{
				return reader.GetInt64(index);
			}
		}

        /// <summary>
        /// Gets the int64 value of column.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <returns></returns>
		protected static long GetInt64Internal(SqlDataReader reader, string columnName)
		{
            int index = GetColumnIndex(reader, columnName);
			return index < 0 ? 0 : GetInt64Internal(reader, index);


		}

        /// <summary>
        /// Gets the date time value of column.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
		protected static DateTime GetDateTimeInternal(SqlDataReader reader, int index)
		{
			if (reader == null)
			{
				throw new ArgumentException("The reader passed to GetIntInternal can not be null");
			}
			if (reader.IsDBNull(index))
			{
				return new DateTime();
			}
			else
			{
				return reader.GetDateTime(index);
			}
		}

        /// <summary>
        /// Gets the date time value of column.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <returns></returns>
		protected static DateTime GetDateTimeInternal(SqlDataReader reader, string columnName)
		{
			int index = GetColumnIndex(reader, columnName);
			return index < 0 ? new DateTime() : GetDateTimeInternal(reader, index);
		}

        /// <summary>
        /// Gets the boolean value of column.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
		protected static Boolean GetBooleanInternal(SqlDataReader reader, int index)
		{
			
				if (reader == null)
				{
					throw new ArgumentException("The reader passed to GetIntInternal can not be null");
				}
				if (reader.IsDBNull(index))
				{
					return false;
				}
				else
				{
					return reader.GetBoolean(index);
				}
		}

        /// <summary>
        /// Gets the boolean value of column.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <returns></returns>
		protected static Boolean GetBooleanInternal(SqlDataReader reader, string columnName)
		{
            int index = GetColumnIndex(reader, columnName);

			return index < 0 ? false : GetBooleanInternal(reader, index);
		}

        /// <summary>
        /// Gets the double value of column.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
		protected static double GetDoubleInternal(SqlDataReader reader, int index)
		{
			if (reader == null)
			{
				throw new ArgumentException("The reader passed to GetStringInternal can not be null");
			}
			if (reader.IsDBNull(index))
			{
				return 0;
			}
			else
			{
				return reader.GetDouble(index);
			}
		}

        /// <summary>
        /// Gets the double value of column.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <returns></returns>
		protected static double GetDoubleInternal(SqlDataReader reader, string columnName)
		{
            int index = GetColumnIndex(reader, columnName);
			return index < 0 ? 0 : GetDoubleInternal(reader, index);
		}

        /// <summary>
        /// Determines whether the specified data record has column.
        /// </summary>
        /// <param name="dr">The data record for the sql result.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <returns>
        /// 	<c>true</c> if the specified dr has column; otherwise, <c>false</c>.
        /// </returns>
        protected static bool HasColumn(IDataRecord record, string columnName)
        {
            // loop through fields to see
            for (int i = 0; i < record.FieldCount; i++)
            {
                // if match return true
                if (record.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }

            // nothing found in loop
            return false;
        }
		#region IDisposable Members

		public void Dispose()
		{
			connection.Close();
			connection.Dispose();
		}

		#endregion
	}
}
