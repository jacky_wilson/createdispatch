using System;
using System.Collections.Generic;
using System.Text;

namespace LevelOne.Core.Data
{
    public class ProspectStatus : GenericData<ProspectStatus>
    {
        public static int NON_PROSPECT = 51;
        public static int PROSPECT = 12;
        public static int INQUIRY = 33;
        public static int VISIT_SET = 37;
        public static int REQUESTED_LIT = 40;
        public static int APP_SU = 5;

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string description;

        public string Description
        {
            get { return description; }
            set { description = value; }
        }
    }

    public class ProspectStatusCollection : GenericDataCollection<ProspectStatus>
    {
    }
}
