using System;
using System.Collections.Generic;
using System.Text;

namespace LevelOne.Core.Data
{
    public class AdSource : GenericData<AdSource>
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string description;

        public string Description
        {
            get { return description; }
            set { description = value; }
        }
    }

    public class AdSourceCollection : GenericDataCollection<AdSource>
    {
    }
}
