﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LevelOne.Core.Data
{
    /// <summary>
    /// User that logins in to one of our applications
    /// </summary>
    public class ApplicationUser : LevelOne.Core.Data.User
    {
        /// <summary>
        /// Gets or sets the portfolio.
        /// </summary>
        /// <value>The portfolio.</value>
        public Portfolio Portfolio
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the property.
        /// </summary>
        /// <value>The property.</value>
        public RentalProperty Property
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the role.
        /// </summary>
        /// <value>The role.</value>
        public string Role
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>The first name.</value>
        public string FirstName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the middle.
        /// </summary>
        /// <value>The name of the middle.</value>
        public string MiddleName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>The last name.</value>
        public string LastName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether login is allowd.
        /// </summary>
        /// <value><c>true</c> if login allowed; otherwise, <c>false</c>.</value>
        public bool AllowLogin
        {
            get;
            set;
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class ApplicationUserCollection : GenericDataCollection<ApplicationUser>
    {
    }
}
