using System;
using System.Collections.Generic;
using System.Text;

namespace LevelOne.Core.Data
{
    public class Prospect : GenericData<Prospect>
    {
        private long custId;

        public long CustId
        {
            get { return custId; }
            set { custId = value; }
        }

        private string firstName;

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        private string lastName;

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        private string streetAddress;

        public string StreetAddress
        {
            get { return streetAddress; }
            set { streetAddress = value; }
        }

        private string aptNo;

        public string AptNo
        {
            get { return aptNo; }
            set { aptNo = value; }
        }

        private string city;

        public string City
        {
            get { return city; }
            set { city = value; }
        }

        private string state;

        public string State
        {
            get { return state; }
            set { state = value; }
        }

        private ZipCode zip;

        public ZipCode Zip
        {
            get { return zip; }
            set { zip = value; }
        }

        private string primaryPhone;

        public string PrimaryPhone
        {
            get { return primaryPhone; }
            set { primaryPhone = value; }
        }

        private string primaryExt;

        public string PrimaryExt
        {
            get { return primaryExt; }
            set { primaryExt = value; }
        }

        private string secondaryPhone;

        public string SecondaryPhone
        {
            get { return secondaryPhone; }
            set { secondaryPhone = value; }
        }

        private string secondaryExt;

        public string SecondaryExt
        {
            get { return secondaryExt; }
            set { secondaryExt = value; }
        }

        private string mobilePhone;

        public string MobilePhone
        {
            get { return mobilePhone; }
            set { mobilePhone = value; }
        }

        private string fax;

        public string Fax
        {
            get { return fax; }
            set { fax = value; }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        private int languageTypeId;

        public int LanguageTypeId
        {
            get { return languageTypeId; }
            set { languageTypeId = value; }
        }

        private int notesId;

        public int NotesId
        {
            get { return notesId; }
            set { notesId = value; }
        }

        private int repId;

        public int RepId
        {
            get { return repId; }
            set { repId = value; }
        }

        private DateTime createDate;

        public DateTime CreateDate
        {
            get { return createDate; }
            set { createDate = value; }
        }

        private int leadTypeId;

        public int LeadTypeId
        {
            get { return leadTypeId; }
            set { leadTypeId = value; }
        }

        private bool isUpdated;

        public bool IsUpdated
        {
            get { return isUpdated; }
            set { isUpdated = value; }
        }

        private int agentId;

        public int AgentId
        {
            get { return agentId; }
            set { agentId = value; }
        }

        private bool isRequestedLit;

        public bool IsRequestedLit
        {
            get { return isRequestedLit; }
            set { isRequestedLit = value; }
        }

        private GuestCard guestCard;

        public GuestCard GuestCard
        {
            get { return guestCard; }
            set { guestCard = value; }
        }

        private string prospectURL;

        public string ProspectURL
        {
            get { return prospectURL; }
            set { prospectURL = value; }
        }

        //LEV-571 additions
        public string middlename { get; set; }
        public string sender_property_id { get; set; }
        public string target_movein_date { get; set; }
        public string floor_plan_code { get; set; }
        public string rent_max { get; set; }



        public override string ToString()
        {
            return String.Format("Prospect[CustID: {0}; FirstName: {1}; LastName: {2}; StreetAddress: {3}; City: {4}; State: {5}; Zip: {6}; ... more]", 
                custId, firstName, lastName, streetAddress, city, state, zip == null ? "null" : zip.ToString(),
                primaryPhone, secondaryPhone);
        }

    }

    public class ProspectCollection : GenericDataCollection<Prospect>
    {
    }

    public class ProspectCreateDateComparer : IComparer<Prospect>
    {

        #region IComparer<Prospect> Members

        public int Compare(Prospect x, Prospect y)
        {
            if (x.CreateDate > y.CreateDate)
            {
                return 1;
            }
            else if (x.CreateDate == y.CreateDate)
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }

        #endregion
    }
}
