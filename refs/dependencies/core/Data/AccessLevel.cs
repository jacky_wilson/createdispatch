using System;
using System.Collections.Generic;
using System.Text;
using LevelOne.Core.Data;

namespace LevelOne.Core.Data
{
   public class AccessLevel : GenericData<AccessLevel>
	{


		#region Privates

		private int _accesslevel_id;
		private string _acessleveldesc;
		private string _role;
		#endregion

		#region Properties

		/// <summary>
		/// Property To Encapsulate the AccessLevel_ID in the Maintenance.dbo.AccessLevels table 
		/// </summary>
		[System.Xml.Serialization.XmlElement("ID")]
		public int Id
		{
			get { return _accesslevel_id; }
			set { _accesslevel_id = value; }
		}

		/// <summary>
		/// Property To Encapsulate the AcessLevelDesc in the Maintenance.dbo.AccessLevels table 
		/// </summary>
		[System.Xml.Serialization.XmlElement("Description")]
		public string Description
		{
			get { return _acessleveldesc; }
			set { _acessleveldesc = value; }
		}

		/// <summary>
		/// Property To Encapsulate the role in the Maintenance.dbo.AccessLevels table 
		/// </summary>
		[System.Xml.Serialization.XmlElement("Role")]
		public string Role
		{
			get { return _role; }
			set { _role = value; }
		}

		#endregion

	}

	/// <summary>
	/// Data Collection Class For the Maintenance.dbo.AccessLevels table 
	/// </summary>
	public class AccessLevelCollection : GenericDataCollection<AccessLevel>
	{
	}
}
