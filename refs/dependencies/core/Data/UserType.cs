using System;
using System.Collections.Generic;
using System.Text;
using LevelOne.Core.Data;

namespace LevelOne.Core.Data
{
     public class UserType : GenericData<UserType>
    {
        private int id;

		[System.Xml.Serialization.XmlElement("UserTypeID")]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string description;

		[System.Xml.Serialization.XmlElement("Description")]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
    }

    public class UserTypeCollection : GenericDataCollection<UserType>
    {
    }
}
