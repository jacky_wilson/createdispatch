using System;
using System.Collections.Generic;
using System.Text;

namespace LevelOne.Core.Data
{
    public class RentalProperty : GenericData<Portfolio>
    {

        public RentalProperty()
        {
        }

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private Portfolio portfolio;

        public Portfolio Portfolio
        {
            get { return portfolio; }
            set { portfolio = value; }
        }

        private bool isSettingVisits;

        public bool IsSettingVisits
        {
            get { return isSettingVisits; }
            set { isSettingVisits = value; }
        }

        private bool isCanceled;

        public bool IsCanceled
        {
            get { return isCanceled; }
            set { isCanceled = value; }
        }

        private bool isActive;

        public bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        private long defaultDID;

        public long DefaultDID
        {
            get { return defaultDID; }
            set { defaultDID = value; }
        }

        private string propertyURL;

        public string PropertyURL
        {
            get { return propertyURL; }
            set { propertyURL = value; }
        }

		private string propertyAddress;

		public string Address
		{
			get { return propertyAddress; }
			set { propertyAddress = value; }
		}

		private string propertyAddress2;

		public string Address2
		{
			get { return propertyAddress2; }
			set { propertyAddress2 = value; }
		}

		private string propertyCity;

		public string City
		{
			get { return propertyCity; }
			set { propertyCity = value; }
		}

		private string propertyState;

		public string State
		{
			get { return propertyState; }
			set { propertyState = value; }
		}

		private string propertyZip;

		public string Zip
		{
			get { return propertyZip; }
			set { propertyZip = value; }
		}

		private string propertyPhone;

		public string Phone
		{
			get { return propertyPhone; }
			set { propertyPhone = value; }
		}

        private string propertyFax;

        public string Fax
        {
            get { return propertyFax; }
            set { propertyFax = value; }
        }


        private string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        private string _adSourceEmail;

        public string AdSourceEmail
        {
            get { return _adSourceEmail; }
            set { _adSourceEmail = value; }
        }

        private bool _hasVisCall;

        public bool HasVisCall
        {
            get { return _hasVisCall; }
            set { _hasVisCall = value; }
        }

        private bool _hashasTime2Time;

        public bool HashasTime2Time
        {
            get { return _hashasTime2Time; }
            set { _hashasTime2Time = value; }
        }

        public TimeZoneInfo TimeZone { get; set; }

    }


    public class RentalPropertyCollection : GenericDataCollection<RentalProperty>
    {
    }
}
