using System;
using System.Collections.Generic;
using System.Text;
using LevelOne.Core.Data;

namespace LevelOne.Core.Data
{
    public class UserPreferences : GenericData<UserPreferences>
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private bool displayMilitaryTime;

        public bool DisplayMilitaryTime
        {
            get { return displayMilitaryTime; }
            set { displayMilitaryTime = value; }
        }

        private int timeSectionID;

        public int TimeSectionID
        {
            get { return timeSectionID; }
            set { timeSectionID = value; }
        }
    }

    public class UserPreferencesCollection : GenericDataCollection<UserPreferences>
    {
    }
}
