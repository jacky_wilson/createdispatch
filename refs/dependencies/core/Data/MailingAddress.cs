using System;
using System.Collections.Generic;
using System.Text;

namespace LevelOne.Core.Data
{
    public class MailingAddress
    {
        private string addressLine1;

        public string AddressLine1
        {
            get { return addressLine1; }
            set { addressLine1 = value; }
        }

        private string addressLine2;

        public string AddressLine2
        {
            get { return addressLine2; }
            set { addressLine2 = value; }
        }

        private string city;

        public string City
        {
            get { return city; }
            set { city = value; }
        }

        private string state;

        public string State
        {
            get { return state; }
            set { state = value; }
        }

        private ZipCode zipcode;

        public ZipCode ZipCode
        {
            get { return zipcode; }
            set { zipcode = value; }
        }


    }
}
