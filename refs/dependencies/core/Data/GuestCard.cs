using System;
using System.Collections.Generic;
using System.Text;

namespace LevelOne.Core.Data
{
    public class GuestCard : GenericData<GuestCard>
    {
        private long custId;

        public long CustId
        {
            get { return custId; }
            set { custId = value; }
        }


        private AdSource adSource;

        public AdSource AdSource
        {
            get { return adSource; }
            set { adSource = value; }
        }

        private LeadType leadType;

        public LeadType LeadType
        {
            get { return leadType; }
            set { leadType = value; }
        }

        private RentalProperty property;

        public RentalProperty RentalProperty
        {
            get { return property; }
            set { property = value; }
        }

        private int callUID;

        public int CallUID
        {
            get { return callUID; }
            set { callUID = value; }
        }

        private string callerID;

        public string CallerID
        {
            get { return callerID; }
            set { callerID = value; }
        }

        private ProspectStatus status;

        public ProspectStatus Status
        {
            get { return status; }
            set { status = value; }
        }

        private int appID;

        public int AppID
        {
            get { return appID; }
            set { appID = value; }
        }

        private DateTime guestCardDelivered;

        public DateTime GuestCardDelivered
        {
            get { return guestCardDelivered; }
            set { guestCardDelivered = value; }
        }

        private int maxID;

        public int MaxID
        {
            get { return maxID; }
            set { maxID = value; }
        }

        private int test;

        public int Test
        {
            get { return test; }
            set { test = value; }
        }

        private bool isAcceptingVisits;

        public bool IsAcceptingVisits
        {
            get { return isAcceptingVisits; }
            set { isAcceptingVisits = value; }
        }

    }

    public class GuestCardCollection : GenericDataCollection<GuestCard>
    {
    }
}

