using System;
using System.Collections.Generic;
using System.Text;

namespace LevelOne.Core.Data
{
    public class LeadType : GenericData<LeadType>
    {
        public static int PHONE = 1;
        public static int EMAIL = 2;
        public static int VAULTWARE = 3;
        public static int MAINT = 4;

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string description;

        public string Description
        {
            get { return description; }
            set { description = value; }
        }
    }

    public class LeadTypeCollection : GenericDataCollection<LeadType>
    {
    }

}

