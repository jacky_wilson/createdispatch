using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace LevelOne.Core.Data
{
	public class GenericData<T> : DataClassBase
	{
	   // Exists for future enhancements and to maintain
	   // symmetry of pattern with other class types
		protected GenericData() : base(){}
	}

	public class GenericDataCollection<T> : List<T>
	{
		public GenericDataCollection()
		{
		}

		public GenericDataCollection(int i) : base(i)
		{
		}
	}


}
