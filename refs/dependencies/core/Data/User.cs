using System;
using System.Collections.Generic;
using System.Text;
using LevelOne.Core.Data;

namespace LevelOne.Core.Data
{
   	[System.Xml.Serialization.XmlRoot("User")]
    public class User : GenericData<User>
    {
        private long id;

		[System.Xml.Serialization.XmlElement("UserID")]
        public long Id
        {
            get { return id; }
            set { id = value; }
        }

        private string username;
		[System.Xml.Serialization.XmlElement("Username")]
        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        private string password;

		[System.Xml.Serialization.XmlIgnore()]
        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        private string contactNumber;

		[System.Xml.Serialization.XmlElement("ContactNumber")]
        public string ContactNumber
        {
            get { return contactNumber; }
            set { contactNumber = value; }
        }

        private int contactExtension;

		[System.Xml.Serialization.XmlIgnore()]
        public int ContactExtension
        {
            get { return contactExtension; }
            set { contactExtension = value; }
        }


        private string email;

		[System.Xml.Serialization.XmlElement("Email")]
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        private AccessLevel accessLevel;

        public AccessLevel AccessLevel
        {
            get { return accessLevel; }
            set { accessLevel = value; }
        }

        private UserType userType;

		[System.Xml.Serialization.XmlElement("UserType")]
        public UserType UserType
        {
            get { return userType; }
            set { userType = value; }
        }

        private int isActive;

		[System.Xml.Serialization.XmlElement("IsActive")]
        public int IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        private int pin;

		[System.Xml.Serialization.XmlElement("Pin")]
        public int Pin
        {
            get { return pin; }
            set { pin = value; }
        }

        private UserPreferences preferences;

        public UserPreferences Preferences
        {
            get { return preferences; }
            set { preferences = value; }
        }

		private string _aliasname;
		/// <summary>
		/// Property To Encapsulate the AliasName in the Maintenance.dbo.Users table 
		/// </summary>
		[System.Xml.Serialization.XmlElement("AliasName")]
		public string AliasName
		{
			get { return _aliasname; }
			set { _aliasname = value; }
		}

		private int _phonetype_id = 0;
		/// <summary>
		/// Property To Encapsulate the PhoneType_ID in the Maintenance.dbo.Users table 
		/// </summary>
		[System.Xml.Serialization.XmlElement("PhoneTypeID")]
		public int PhoneTypeID
		{
			get { return _phonetype_id; }
			set { _phonetype_id = value; }
		}

		private bool isInActiveCascade = true;
		/// <summary>
		/// Used by the CallOrderAssignment to show that the user was or was not included in the activated cascade.
		/// If you modify a cascade and exit without activating the user will not be pushed in. This is here so that any 
		/// questions that come up concerning why a user is not being called in a cascade .....
		/// </summary>
		[System.Xml.Serialization.XmlElement("IsInActiveCascade")]
		public bool IsInActiveCascade
		{
			get{return isInActiveCascade;}
			set {isInActiveCascade = value;}

		}

    }

	/// <summary>
	/// Enumerated list of phonetypes Replace this with a PhoneTypeDAL AND BAL?
	/// </summary>
	public enum PhoneType
	{
		none = 0,
		Home = 1,
		Mobile = 2,
		Pager = 3,
		Other = 4
	}

    public class UserCollection : GenericDataCollection<User>{ }
}
