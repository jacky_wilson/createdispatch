﻿using System;
using System.Collections.Generic;
using System.Text;
using LevelOne.Core.Data;

namespace LevelOne.Core.Data
{
    public class Agent : GenericData<Agent>
    {
        private long id;
        private string logon;
        private string firstName;
        private string middleName;
        private string lastName;
        private string suffix;
        private string email;
        private string role;

        public Agent() { }

        public long Id
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }

        public string Username
        {
            get
            {
                return this.logon;

            }
            set
            {
                this.logon = value;
            }
        }

        public string FirstName
        {
            get
            {
                return this.firstName;
            }
            set
            {
                this.firstName = value;
            }
        }

        public string MiddleName
        {
            get
            {
                return this.middleName;
            }
            set
            {
                this.middleName = value;
            }
        }

        public string LastName
        {
            get
            {
                return this.lastName;
            }
            set
            {
                this.lastName = value;
            }
        }

        public string Suffix
        {
            get
            {
                return this.suffix;
            }
            set
            {
                this.suffix = value;
            }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string Role
        {
            get { return role; }
            set { role = value; }
        }
    }

    public class AgentCollection : GenericDataCollection<Agent>
    { }


}
