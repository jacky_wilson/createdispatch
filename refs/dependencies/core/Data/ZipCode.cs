using System;
using System.Collections.Generic;
using System.Text;

namespace LevelOne.Core.Data
{
    public class ZipCode
    {
        private string zipCode;
        private string fiveDigitZip;
        private string plus4;

        public string ZipCodeValue
        {
            get { return zipCode; }
            set 
            {
                int numericValue;

                if (value == null)
                {
                    // duh
                    throw new Exception("Invalid Zip Code: Value can not be null");
                }
                // no characters
                else if(Int32.TryParse(value, out numericValue))
                {
                    // normal five digit zip. all zips are five digits, but b/c
                    // of some systems storing as int's the preceding zeros are
                    // trimmed by the object type. We need to put them back.
                    // The same goes for plus 4.
                    if (value.Length < 5)
                    {
                        value.PadLeft(5, '0');
                        zipCode = value;
                        fiveDigitZip = value;
                    }
                    else
                    {
                        fiveDigitZip = value.Substring(0,5);
                        plus4 = value.Substring(5).PadLeft(4, '0');
                        zipCode = fiveDigitZip + plus4;                        
                    }
                }
                // in case the value is formatted nine digit zip
                else if (value.IndexOf("-") > 0)
                {
                    int dashIndex = value.IndexOf("-");
                    fiveDigitZip = value.Substring(0, dashIndex + 1).PadLeft(5, '0');
                    plus4 = value.Substring(dashIndex + 1).PadLeft(4, '0');
                    zipCode = fiveDigitZip + plus4;     
                }
                else
                {
                    throw new Exception("Invalid Zip Code: " + value);
                }
            }
        }

        public string FormattedZip
        {
            get { return zipCode == null ?  "" : 
                                            zipCode.Length > 5 ? String.Format("{0}-{1}", fiveDigitZip, plus4) : zipCode;
                }
        }

        public string FiveDigitZip
        {
            get
            {
                if (zipCode != null)
                {
                    return zipCode.Substring(0, 4);
                }
                else
                {
                    return null;
                }
            }
        }

        public string Plus4
        {
            get
            {
                if (zipCode != null && zipCode.Length > 5)
                {
                    return zipCode.Substring(0, 4);
                }
                else
                {
                    return null;
                }
            }
        }

        public override string ToString()
        { 
            return zipCode == null ? "null" : zipCode.Length > 5 ? 
                    zipCode.Substring(0, 4) + "-" + zipCode.Substring(5) : zipCode;
                    
        }
    }
}
