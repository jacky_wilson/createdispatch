using System;
using System.Collections.Generic;
using System.Text;

namespace LevelOne.Core.Data
{
    public class Portfolio : GenericData<Portfolio>
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
    }

    public class PortfolioCollection : GenericDataCollection<Portfolio>
    {
    }
}
