using System;
using System.Collections.Generic;
using System.Text;

namespace LevelOne.Core.Data
{
	public class PhoneNumber
	{

		private int _areaCode;
		private int _exchange;
		private string _destination;
		private string _extension;

		/// <summary>
		/// Sets Or Gets the AreaCode
		/// </summary>
		public int AreaCode
		{
			get{return _areaCode;}
			set
			{

				int numericValue;
				
				if (Int32.TryParse(value.ToString(), out numericValue))
				{
					if (_areaCode < 1000)
					{
						_areaCode = value;
					}
				}
				else
				{
					throw new Exception("Area code is not numeric");
				}
			}
		}

		/// <summary>
		/// Gets or Sets the Exchange Code
		/// </summary>
		public int Exchange
		{
			get { return _exchange; }
			set
			{

				int numericValue;

				if (Int32.TryParse(value.ToString(), out numericValue))
				{
					if (_exchange < 1000)
					{
						_exchange = value;
					}
				}
				else
				{
					throw new Exception("Exchange code is not numeric");
				}
			}
		}

		/// <summary>
		/// Gets or Sets the Destination
		/// </summary>
		public string Destination
		{
			get { return _destination; }
			set
			{

				int numericValue;

				if (Int32.TryParse(value.ToString(), out numericValue))
				{
					if (value.Length < 5)
					{
						_destination = value;
					}
				}
				else
				{
					throw new Exception("Destination code is not numeric");
				}
			}
		}

		/// <summary>
		/// Gets or Sets the Extension
		/// </summary>
		public string Extension
		{
			get { return _extension; }
			set { _extension = value; }
		}

		/// <summary>
		/// TryParse for a phone number
		/// </summary>
		/// <param name="phoneNumber">string of phone number (999)-999-9999 or (999)-999-9999-9999 or 999-9999</param>
		/// <param name="phone">out PhoneNumber</param>
		/// <returns>true if parse was successful</returns>
		public static bool TryParse(string phoneNumber, out PhoneNumber phone)
		{
			//create the out object
			bool retVal = false;
			phone = new PhoneNumber();
			//remove the (
			phoneNumber = phoneNumber.Replace("(", "");

			//remove the )
			phoneNumber = phoneNumber.Replace(")", "");

			//split the phone number on the -
			string[] _splPhone = phoneNumber.Split("-".ToCharArray()[0]);

			if (_splPhone.Length != 0)
			{
				int areacode = 0;
				int exchange = 0;
				string destination = "0";
				string extension = "0";

				//build the phone number
				switch (_splPhone.Length)
				{
					case 2:
						//if the length ==1 then it is just exchange and destination
						
						int.TryParse(_splPhone[0].ToString(), out exchange);
						//int.TryParse(_splPhone[1].ToString(), out destination);
						destination = _splPhone[1].ToString();

						if ((exchange != 0) && (destination.Length != 0))
						{
							phone.Exchange = exchange;
							phone.Destination = destination;
							retVal = true;
						}
						

						break;

					case 3:
						//if the length == 3 then there is an area code
						int.TryParse(_splPhone[0].ToString(), out areacode);
						int.TryParse(_splPhone[1].ToString(), out exchange);
						destination = _splPhone[2].ToString();
						//int.TryParse(_splPhone[2].ToString(), out destination);

						if ((areacode != 0) && (exchange != 0) && (destination.Length != 0))
						{
							phone.AreaCode = areacode;
							phone.Exchange = exchange;
							phone.Destination = destination;
							retVal = true;
						}
						
					

						break;
					case 4:
						//if the length == 3 then there is an ext
						//if the length == 3 then there is an area code
						int.TryParse(_splPhone[0].ToString(), out areacode);
						int.TryParse(_splPhone[1].ToString(), out exchange);
						destination = _splPhone[2].ToString();
						extension = _splPhone[3].ToString();
						
						if ((areacode != 0) && (exchange != 0) && (destination.Length != 0) && (extension.Length != 0))
						{
							phone.AreaCode = areacode;
							phone.Exchange = exchange;
							phone.Destination = destination;
							phone.Extension = extension;
							retVal = true;
						}
						break;
					default:
							phone = null;
							break;
				}

			}

			if (!(retVal))
			{
				phone = null;
			}
			return retVal;

		}

		/// <summary>
		/// Returns the PhoneNumber formated
		/// </summary>
		/// <returns>string</returns>
		public override string ToString()
		{
			//build the phone number up
			//return base.ToString();
			StringBuilder sb = new StringBuilder();
			string strAreaCode = "";
			string strExchange = "";
			string strDestination = "";
			string strExtension = "";



			if (this.AreaCode != 0)
			{
				strAreaCode = String.Format("({0})-", this.AreaCode);
			}
			else
			{
				strAreaCode = "(000)-";
			}

			if (this.Exchange != 0)
			{
				strExchange = String.Format("{0}-", this.Exchange);
			}
			else
			{
				strExchange = "(000)-";
				
			}

			if ((this.Destination != null) && (this.Destination.Length != 0))
			{
				strDestination = String.Format("{0}", this.Destination);
			}
			else
			{
				strDestination = "0000";
			}

			if ((this.Extension != null) && (this.Extension.Length != 0))
			{
				strExtension = String.Format("-{0}", this.Extension);
			}
			return String.Format("{0}{1}{2}{3}", strAreaCode, strExchange, strDestination, strExtension);
		}

        /// <summary>
        /// Return an all numeric form of the phone number
        /// </summary>
        /// <returns></returns>
        public int toInt32()
        {
            int val = 0;

            StringBuilder strb = new StringBuilder();
            strb.Append(_areaCode);
            strb.Append(_exchange);
            strb.Append(_destination);
            if (!Int32.TryParse(strb.ToString(), out val))
            {
                val = 0;
            }

            return val;
        }

		/// <summary>
		/// Takes long representation of the phone number and formats to (000)-000-0000
		/// </summary>
		/// <param name="did">long</param>
		/// <returns>formated phone number</returns>
		public static string ParseDIDToHumanReadable(long did)
		{
			string phone = did.ToString();
			if (phone.Length == 10)
			{
				char[] phoneArray = phone.ToCharArray();

				if(phoneArray.Length == 10)
				{
					phone = String.Format("({0}{1}{2})-{3}{4}{5}-{6}{7}{8}{9}",
						phoneArray[0].ToString(),
						phoneArray[1].ToString(),
						phoneArray[2].ToString(),
						phoneArray[3].ToString(),
						phoneArray[4].ToString(),
						phoneArray[5].ToString(),
						phoneArray[6].ToString(),
						phoneArray[7].ToString(),
						phoneArray[8].ToString(),
						phoneArray[9].ToString()
						);
				}
			}
			return phone;
		}


		/// <summary>
		/// Formats the PhoneNumber object into the given format
		/// </summary>
		/// <param name="phn">PhoneNumber</param>
		/// <param name="formatedType">PhoneNumberFormat</param>
		/// <returns>fornatted string</returns>
		public static string Format(PhoneNumber phn, PhoneNumberFormat formatedType)
		{
			string retVal = String.Empty;

			if (phn != null)
			{
				switch (formatedType)
				{
					case PhoneNumberFormat.IVR:
						if ((phn.AreaCode != 0) && (phn.Exchange != 0) && (phn.Destination.Length != 0))
						{
							string _areacode = phn.AreaCode.ToString();
							string _exchange = phn.Exchange.ToString();
							string _destination = phn.Destination.ToString();

							//check  to make sure we have a 
							if ((_areacode.Length == 3) && (_exchange.Length == 3) && (_destination.Length == 4))
							{
								retVal = String.Format("{0}{1}{2}", _areacode, _exchange, _destination);
							}//check lengths
						}

						break;
					case PhoneNumberFormat.HumanReadable:
						if ((phn.AreaCode != 0) && (phn.Exchange != 0) && (phn.Destination.Length != 0))
						{
							string _areacode = phn.AreaCode.ToString();
							string _exchange = phn.Exchange.ToString();
							string _destination = phn.Destination.ToString();

							//check  to make sure we have a 
							if ((_areacode.Length == 3) && (_exchange.Length == 3) && (_destination.Length== 4))
							{
								retVal = String.Format("({0})-{1}-{2}", _areacode, _exchange, _destination);

								if (phn.Extension.Length != 0)
								{
									retVal += String.Format("-{0}", phn.Extension);
								}//phn.Extension 
								
							}//check lengths
						}

						break;

				}//switch (formated..

			}//phn != null

			return retVal;
		}//Format

	}


	/// <summary>
	/// Phone Number Format Enumeration
	/// </summary>
	public enum PhoneNumberFormat
	{
		IVR = 0,
		HumanReadable = 1
	}
}
