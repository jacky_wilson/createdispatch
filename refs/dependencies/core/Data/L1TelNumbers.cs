using System;
using System.Collections.Generic;
using System.Text;

namespace LevelOne.Core.Data
{
	/// <summary>
	/// Data Class For the Util.dbo.L1TelNumbers table 
	/// </summary>
	public class L1TelNumbers : GenericData<L1TelNumbers>
	{


		#region Privates

		private int _numberid;
		private string _client_identifier;
		private int _propertyid;
		private string _nuvox_toll_free;
		private string _did_number;
		private int _intinuse;
		private int _intoutboundcode;
		private int _adsourceid;
		private bool _bisdefault;
		private long _bidid;
		private bool _badditionaldid;
		private long _bitollfree;
		private int _mgmtcoid;
		#endregion

		#region Properties

		/// <summary>
		/// Property To Encapsulate the NumberID in the Util.dbo.L1TelNumbers table 
		/// </summary>
		[System.Xml.Serialization.XmlElement("NumberID")]
		public int NumberID
		{
			get { return _numberid; }
			set { _numberid = value; }
		}

		/// <summary>
		/// Property To Encapsulate the Client_Identifier in the Util.dbo.L1TelNumbers table 
		/// </summary>
		[System.Xml.Serialization.XmlElement("ClientIdentifier")]
		public string ClientIdentifier
		{
			get { return _client_identifier; }
			set { _client_identifier = value; }
		}

		/// <summary>
		/// Property To Encapsulate the PropertyID in the Util.dbo.L1TelNumbers table 
		/// </summary>
		[System.Xml.Serialization.XmlElement("PropertyID")]
		public int PropertyID
		{
			get { return _propertyid; }
			set { _propertyid = value; }
		}

		/// <summary>
		/// Property To Encapsulate the Nuvox_Toll_Free in the Util.dbo.L1TelNumbers table 
		/// </summary>
		[System.Xml.Serialization.XmlElement("NuvoxTollFree")]
		public string NuvoxTollFree
		{
			get { return _nuvox_toll_free; }
			set { _nuvox_toll_free = value; }
		}

		/// <summary>
		/// Property To Encapsulate the DID_Number in the Util.dbo.L1TelNumbers table 
		/// </summary>
		[System.Xml.Serialization.XmlElement("DIDNumber")]
		public string DIDNumber
		{
			get { return _did_number; }
			set { _did_number = value; }
		}

		/// <summary>
		/// Property To Encapsulate the intInUse in the Util.dbo.L1TelNumbers table 
		/// </summary>
		[System.Xml.Serialization.XmlElement("IntInUse")]
		public int IntInUse
		{
			get { return _intinuse; }
			set { _intinuse = value; }
		}

		/// <summary>
		/// Property To Encapsulate the intOutboundCode in the Util.dbo.L1TelNumbers table 
		/// </summary>
		[System.Xml.Serialization.XmlElement("IntOutboundCode")]
		public int IntOutboundCode
		{
			get { return _intoutboundcode; }
			set { _intoutboundcode = value; }
		}

		/// <summary>
		/// Property To Encapsulate the AdSourceID in the Util.dbo.L1TelNumbers table 
		/// </summary>
		[System.Xml.Serialization.XmlElement("AdSourceID")]
		public int AdSourceID
		{
			get { return _adsourceid; }
			set { _adsourceid = value; }
		}

		/// <summary>
		/// Property To Encapsulate the bIsDefault in the Util.dbo.L1TelNumbers table 
		/// </summary>
		[System.Xml.Serialization.XmlElement("BIsDefault")]
		public bool BIsDefault
		{
			get { return _bisdefault; }
			set { _bisdefault = value; }
		}

		/// <summary>
		/// Property To Encapsulate the biDID in the Util.dbo.L1TelNumbers table 
		/// </summary>
		[System.Xml.Serialization.XmlElement("BiDID")]
		public long BiDID
		{
			get { return _bidid; }
			set { _bidid = value; }
		}

		/// <summary>
		/// Property To Encapsulate the bAdditionalDID in the Util.dbo.L1TelNumbers table 
		/// </summary>
		[System.Xml.Serialization.XmlElement("BAdditionalDID")]
		public bool BAdditionalDID
		{
			get { return _badditionaldid; }
			set { _badditionaldid = value; }
		}

		/// <summary>
		/// Property To Encapsulate the biTollFree in the Util.dbo.L1TelNumbers table 
		/// </summary>
		[System.Xml.Serialization.XmlElement("BiTollFree")]
		public long BiTollFree
		{
			get { return _bitollfree; }
			set { _bitollfree = value; }
		}

		/// <summary>
		/// Property To Encapsulate the MgmtCoID in the Util.dbo.L1TelNumbers table 
		/// </summary>
		[System.Xml.Serialization.XmlElement("MgmtCoID")]
		public int MgmtCoID
		{
			get { return _mgmtcoid; }
			set { _mgmtcoid = value; }
		}

		#endregion

	}

	/// <summary>
	/// Data Collection Class For the Util.dbo.L1TelNumbers table 
	/// </summary>
	public class L1TelNumbersCollection : GenericDataCollection<L1TelNumbers>
	{
	}

}
