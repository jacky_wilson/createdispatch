using System;
using System.Collections.Generic;
using System.Text;
using LevelOne.Core.DAL;
using LevelOne.Core.Data;

namespace LevelOne.Core.BAL
{
	public class GenericBALBase<TData, TDataCollection, TDal> : BALBase
		where TDal : GenericDalBase<TData, TDataCollection>
		where TData : DataClassBase
		where TDataCollection : List<TData> 
	{
		// Code analysis would like this to be static, but that is not consistent 
		// with its use in the generic version
		public TDataCollection GetList()
		{
			// Use a little reflection to instantiate the dal and data
			TDal dal = System.Activator.CreateInstance(typeof(TDal), true) as TDal;
			return dal.GetList();
		}

        // Code analysis would like this to be static, but that is not consistent 
		// with its use in the generic version
		public TDataCollection GetList(string connectionString)
		{
			// Use a little reflection to instantiate the dal and data
			TDal dal = System.Activator.CreateInstance(typeof(TDal), true) as TDal;
			return dal.GetList(connectionString);

		}

		// Code analyiss would like this to be static, but that is not consistent 
		// with its use in the generic version
        public TData GetData(int id)
        {
            // Use a little reflection to instantiate the dal and data
            TDal dal = System.Activator.CreateInstance(typeof(TDal)) as TDal;
            return dal.GetData(id);
        }

        public TData GetData(long id)
        {
            TDal dal = System.Activator.CreateInstance(typeof(TDal)) as TDal;
            return dal.GetData(id);
        }

        public int Insert(TData data)
        {
            // Use a little reflection to instantiate the dal and data
            TDal dal = System.Activator.CreateInstance(typeof(TDal)) as TDal;
            return dal.Insert(data);
        }

        public int Insert(TData data, int userId)
        {
            // Use a little reflection to instantiate the dal and data
            TDal dal = System.Activator.CreateInstance(typeof(TDal)) as TDal;
            return dal.Insert(data, userId);
        }

        public int Insert(TData data, long userId)
        {
            // Use a little reflection to instantiate the dal and data
            TDal dal = System.Activator.CreateInstance(typeof(TDal)) as TDal;
            return dal.Insert(data, userId);
        }

        
        //public override long Insert(TData data)
        //{
        //    TDal dal = System.Activator.CreateInstance(typeof(TDal)) as TDal;
        //    return dal.Insert(data);
        //}

		public void Update(TData data)
		{
			// Use a little reflection to instantiate the dal and data
			TDal dal = System.Activator.CreateInstance(typeof(TDal)) as TDal;
			dal.Update(data);
		}

        public void Update(TData data, int userId)
        {
            // Use a little reflection to instantiate the dal and data
            TDal dal = System.Activator.CreateInstance(typeof(TDal)) as TDal;
            dal.Update(data, userId);
        }

        public void Update(TData data, long userId)
        {
            // Use a little reflection to instantiate the dal and data
            TDal dal = System.Activator.CreateInstance(typeof(TDal)) as TDal;
            dal.Update(data, userId);
        }

        public void Delete(int id)
        {
            // Use a little reflection to instantiate the dal and data
            TDal dal = System.Activator.CreateInstance(typeof(TDal)) as TDal;
            dal.Delete(id);
        }

        public void Delete(int id, int userId)
        {
            // Use a little reflection to instantiate the dal and data
            TDal dal = System.Activator.CreateInstance(typeof(TDal)) as TDal;
            dal.Delete(id, userId);
        }

        public void Delete(int id, long userId)
        {
            // Use a little reflection to instantiate the dal and data
            TDal dal = System.Activator.CreateInstance(typeof(TDal)) as TDal;
            dal.Delete(id, userId);
        }


        public void Delete(long id)
        {
            TDal dal = System.Activator.CreateInstance(typeof(TDal)) as TDal;
            dal.Delete(id);
        }

        public void Delete(long id, int userId)
        {
            TDal dal = System.Activator.CreateInstance(typeof(TDal)) as TDal;
            dal.Delete(id, userId);
        }

        public void Delete(long id, long userId)
        {
            TDal dal = System.Activator.CreateInstance(typeof(TDal)) as TDal;
            dal.Delete(id, userId);
        }

	}
}


