﻿using System;
using CreateDispatch.Model;
using System.Data.SqlClient;
using System.Configuration;
using CreateDispatch.BAL;
using LevelOne.Core.DAL;
using Bugsnag.Clients;
using System.Data;

namespace CreateDispatch.DAL
{
    /// <summary>
    /// 
    /// </summary>
    public class DispatchDAL
    {

        string connectionString = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        /// <summary>
        /// Dispatches this instance.
        /// </summary>
        public void Dispatch(Dispatch dispatch)
        {
            bool emergency = false;
            int calltype = 27;
            int i = DispatchBAL.IsEmergency(dispatch.Issue, GetEmergencyList(dispatch.Account_ID));
            if (i > 0)
            {
                emergency = true;
                calltype = 26;
            }
            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand comm = new SqlCommand();
            try
            {
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Connection = conn;
                comm.CommandText = "Maintenance.dbo.CreateDispatch";
                conn.Open();
                comm.Parameters.Add(new SqlParameter("@Account_ID", dispatch.Account_ID));
                comm.Parameters.Add(new SqlParameter("@Address_ID", "0"));
                comm.Parameters.Add(new SqlParameter("@AgentID", 15029));
                comm.Parameters.Add(new SqlParameter("@Alarm", dispatch.Alarm));
                comm.Parameters.Add(new SqlParameter("@BuildingNumber", dispatch.BuildingNumber));
                comm.Parameters.Add(new SqlParameter("@CallType", calltype));
                comm.Parameters.Add(new SqlParameter("@CallUID", dispatch.CallUID));
                comm.Parameters.Add(new SqlParameter("@Comments", dispatch.Comments));
                comm.Parameters.Add(new SqlParameter("@ContactEmail", dispatch.ContactEmail));
                comm.Parameters.Add(new SqlParameter("@Emergency", emergency));
                comm.Parameters.Add(new SqlParameter("@Emergency_ID", i));
                comm.Parameters.Add(new SqlParameter("@IssueLocation_ID", "0"));
                comm.Parameters.Add(new SqlParameter("@Permission", dispatch.Permission));
                comm.Parameters.Add(new SqlParameter("@Pets", dispatch.Pets));
                comm.Parameters.Add(new SqlParameter("@PreAddress", ""));
                comm.Parameters.Add(new SqlParameter("@ResidentFirstName", dispatch.ResidentFirstName));
                comm.Parameters.Add(new SqlParameter("@ResidentLastName", dispatch.ResidentLastName));
                comm.Parameters.Add(new SqlParameter("@ResidentTelephone", dispatch.ResidentTelephone));
                comm.Parameters.Add(new SqlParameter("@UnitNumber", dispatch.UnitNumber));
                comm.Parameters.Add(new SqlParameter("@WorkOrder", ""));

                comm.ExecuteNonQuery();
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                conn.Close();
            }
        }
        /// <summary>
        /// Gets the emergency list.
        /// </summary>
        /// <param name="AccountID">The account identifier.</param>
        /// <returns></returns>
        public DataTable GetEmergencyList(int AccountID)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand comm = new SqlCommand();
            DataTable elist = new DataTable();
            elist.Clear();

            comm.CommandType = System.Data.CommandType.Text;
            comm.Connection = conn;
            comm.CommandText = "select Emergency_ID, EmergencyDesc from Maintenance.dbo.DefaultEmergencyList where DefaultItem = 1";
            conn.Open();
            comm.Parameters.Add(new SqlParameter("@Account_ID", AccountID));
            using (SqlDataAdapter a = new SqlDataAdapter(comm))
            {
                a.Fill(elist);
            }
            return elist;
            //If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            //    With rblEmergencyList
            //        .DataSource = ds
            //        .DataTextField = "ReasonDesc"
            //        .DataValueField = "Emergency_ID"
            //        .DataBind()

        }

    }
}