﻿using System.Net;
using System.Net.Http;
using System.ServiceModel.Channels;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace SwaggerTemplate
{
    public class SwaggerAccessMessageHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (IsSwagger(request) && IsRealPageRequest(request))
            {
                var response = request.CreateResponse(HttpStatusCode.Unauthorized);
                return Task.FromResult(response);
            }
            else
            {
                return base.SendAsync(request, cancellationToken);
            }
        }

        private  bool IsRealPageRequest(HttpRequestMessage request)
        {
            if (request.RequestUri.Host == "localhost") { return true; }

#if DEBUG
            return true;
#endif

            return false;
        }

        private bool IsSwagger(HttpRequestMessage request)
        {
            return request.RequestUri.PathAndQuery.Contains("/swagger");
        }
    }
}