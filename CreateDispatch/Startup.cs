﻿using IdentityServer3.AccessTokenValidation;
using Microsoft.Owin;
using Owin;
using System.Web.Http;

[assembly: OwinStartup(typeof(SwaggerTemplate.Startup))]

namespace SwaggerTemplate
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
           
            // configure web api
            var config = new HttpConfiguration();

            config.MapHttpAttributeRoutes();
            config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;

            
            app.UseWebApi(config);

            SwaggerConfig.Register(config);

            
        }
    }
}
