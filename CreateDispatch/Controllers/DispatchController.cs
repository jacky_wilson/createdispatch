﻿using System.Web.Http;
using CreateDispatch.Model;

using CreateDispatch.DAL;
using System;

namespace CreateDispatch.Controllers
{
    /// <summary>
    /// Controller to create a dispatch
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    [RoutePrefix("Dispatch")]
    public class DispatchController : ApiController
    {

        /// <summary>
        /// Creates the specified Dispatch.
        /// </summary>
        /// <param name="dispatch">The Dispatch.</param>
        /// <returns>demo</returns>
        [HttpPost]
        [Route("Create")]
        public IHttpActionResult CreateDispatch ([FromBody] Dispatch dispatch)
        {
            try
            {
                DispatchDAL dispatchDal = new DispatchDAL();
                dispatchDal.Dispatch(dispatch);
                return Ok(dispatch);
            }
            catch(Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}