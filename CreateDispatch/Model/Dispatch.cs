﻿

namespace CreateDispatch.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class Dispatch
    {

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public int Account_ID { get; set; }
        /// <summary>
        /// Gets or sets the agent identifier.
        /// </summary>
        /// <value>
        /// The agent identifier.
        /// </value>
    
      
        public string ResidentFirstName { get; set; }
        /// <summary>
        /// Gets or sets the last name of the resident.
        /// </summary>
        /// <value>
        /// The last name of the resident.
        /// </value>
        public string ResidentLastName { get; set; }
        /// <summary>
        /// Gets or sets the unit number.
        /// </summary>
        /// <value>
        /// The unit number.
        /// </value>
        public string UnitNumber { get; set; }
        /// <summary>
        /// Gets or sets the resident telephone.
        /// </summary>
        /// <value>
        /// The resident telephone.
        /// </value>
        public string ResidentTelephone { get; set; }
        /// <summary>
        /// Gets or sets the contact email.
        /// </summary>
        /// <value>
        /// The contact email.
        /// </value>
        public string ContactEmail { get; set; }
        /// <summary>
        /// Gets or sets the permission.
        /// </summary>
        /// <value>
        /// The permission.
        /// </value>
        public bool Permission { get; set; }
        /// <summary>
        /// Gets or sets the emergency.
        /// </summary>
        /// <value>
        /// The emergency.
        /// </value>
     
        public string Comments { get; set; }
        /// <summary>
        /// Gets or sets the work order.
        /// </summary>
        /// <value>
        /// The work order.
        /// </value>
    
       
        public string BuildingNumber { get; set; }
        /// <summary>
        /// Gets or sets the pets.
        /// </summary>
        /// <value>
        /// The pets.
        /// </value>
        public bool Pets { get; set; }
        /// <summary>
        /// Gets or sets the alarm.
        /// </summary>
        /// <value>
        /// The alarm.
        /// </value>
        public bool Alarm { get; set; }




        /// <summary>
        /// Gets or sets the issue.
        /// </summary>
        /// <value>
        /// The issue.
        /// </value>
        public string Issue { get; set;  }

        /// <summary>
        /// Gets or sets the call uid.
        /// </summary>
        /// <value>
        /// The call uid.
        /// </value>
        public int CallUID { get; set; }
    }


}