using Swashbuckle.Application;
using Swashbuckle.Swagger;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Services.Description;
using SwaggerTemplate.Properties;

namespace SwaggerTemplate
{
    public class SwaggerConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            //config.MessageHandlers.Add(new SwaggerAccessMessageHandler());

            config
                .EnableSwagger(c =>
                    {
                        c.SingleApiVersion("v1", "CreateDispatch API");
                        c.IncludeXmlComments(Settings.Default.XmlComments);
                        c.RootUrl(req =>
                            req.RequestUri.GetLeftPart(System.UriPartial.Authority)
                            + System.Web.VirtualPathUtility.ToAbsolute("~").TrimEnd('/'));
                    })
                .EnableSwaggerUi(c =>
                    {
                    });
        }
    }
}
