﻿using System;
using CreateDispatch.Model;
using System.Data.SqlClient;
using System.Configuration;

using LevelOne.Core.DAL;
using Bugsnag.Clients;
using System.Data;

namespace CreateDispatch.BAL
{
    /// <summary>
    /// 
    /// </summary>
    public class DispatchBAL
    {
        /// <summary>
        /// Determines whether the specified issue is emergency.
        /// </summary>
        /// <param name="Issue">The issue.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        static internal int IsEmergency(string Issue, DataTable dt)
        {
            int i = 0;
            foreach (DataRow row in dt.Rows)
            {
                if(row["EmergencyDesc"].ToString().ToUpper().Contains(Issue.ToUpper()))
                {
                    i = Convert.ToInt32(row["Emergency_ID"]);
                }
            }
            return i;           
            
        }

    }
}